const {createApp} = Vue;

createApp({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens Locais
            imagensLocais: [
                './images/lua.jpg',
                './images/SENAI_logo.png',
                './images/sol.jpg'
            ],

            imagesInternet:[
                'http://autoetecnica.band.uol.com.br/wp-content/uploads/2018/10/Dodge-Demon_925x520_acf_cropped.jpg',
                'https://uploads.metropoles.com/wp-content/uploads/2022/05/14122500/GettyImages-1397138444.jpg',
                'https://transfernews.com.br/wp-content/uploads/2023/03/Raphael-Veiga-Palmeiras.jpg'
            ],


        };//fim return
    },//fim data

    computed:{
        randomImage(){
            return this.imagensLocais[this.randomIndex];
        },//fim randomImage
        randomImageInternet(){
            return this.imagesInternet[this.randomIndexInternet];
        }
    },//fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length);
            this.randomIndexInternet = Math.floor(Math.random()*this.imagesInternet.length);
        }
    }
}).mount("#app");
