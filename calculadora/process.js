const {createApp} = Vue;
createApp({
    data()
    {
        return{
            display:"0",
            operandoAtual: null,
            operandoAnterior: null,
            operador:null,    
    }
},//Fechamento da função "data"

methods:{
    handleButtonClick(botao){
        switch(botao){
            case "+":
            case "-":
            case "/":
            case "*":
                this.handleOperation(botao);
                break;

            case "=":
                this.handleEquals();
                break;
            
            default: 
                this.handleNumber(botao);
                break;
        
        }
    },

    handleNumber(numero){
        if(this.display === "0")
        {
            this.display = numero.toString();
        }

        else
        {
            this.display += numero.toString();
        }

    },
    
    handleOperation(operacao){
        if(this.operandoAtual !== null){
        
            this.handleEquals();
        
        }//Fechamento de if

        this.operador = operacao;

        this.operandoAtual = parseFloat(this.display);

        this.display = "0";

    },

    handleEquals(){
        
        const displayAtual = parseFloat(this.display);
        console.log(this.operador);
        if((this.operandoAtual !== null) && (this.operador !== null)){
            console.log("entrou");
            switch(this.operador){
                case "+":
                    console.log("testw");
                    this.display = (this.operandoAtual + displayAtual).toString();
                    break;
                case "-":
                    this.display = (this.operandoAtual - displayAtual).toString();
                    break;
                case "*":
                    this.display = (this.operandoAtual * displayAtual).toString();
                    break;
                case "/":
                    this.display = (this.operandoAtual / displayAtual).toString();
                    break;
        }//(Fechamento switch

                this.operandoAnterior = this.operandoAtual;
                this.operador = null;
                this.operandoAtual = null;
                
        }//Fechamento do if
        else
        {
            this.operandoAnterior = displayAtual;
        }//Fechamento do else
    },//Fechamento handleEqual


    handleDecimal(){
        if(!this.display.includes("."))
        {
            this.display += ".";
        }//Fechamento if
    }, //Fechamento handleDecimal

    handleClear(){
        this.display = "0";
        this.operandoAtual = null; 
        this.operandoAnterior = null;
    },//Fechamento handleClear

}

}).mount("#app");//Fechamento do "createApp"    

